#!/usr/bin/env bash

echo "Starting bootstrapping"

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
    echo "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update homebrew recipes
brew update

PACKAGES=(
    wget
    bash-completion
    #git
    openssl
    #vim
    #node
    #yarn
    #zsh
    htop
)

echo "Installing packages ..."
brew install ${PACKAGES[@]}

## Install GNU core utilities (those that come with OS X are outdated)
#brew install coreutils
#brew install gnu-sed --with-default-names
#brew install gnu-tar --with-default-names
#brew install gnu-indent --with-default-names
#brew install gnu-which --with-default-names
#brew install grep --with-default-names

## Install GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
#brew install findutils

echo "Installing cask ..."
brew tap caskroom/cask

CASKS=(
    iterm2
    google-chrome
    barrier
    divvy
    textmate
    iina
    firefox
    opera
    appcleaner
    numi
    #slack
    #spotify
    #visual-studio-code
    #vlc
    #1password
    #caffeine
    #docker
    #hyper
    #kitematic
    #macvim
    #synergy
    #dropbox
    #simplenote
    #nextcloud
    #qbittorrent
    #the-unarchiver
    #transmission-remote-gui
    #telegram-desktop
    #dropbox
    #authy
    #box-sync
    #nextcloud
    #suspicious-package
    #vagrant
    #virtualbox
    #teamviewer
    #nrlquaker-winbox
    #cryptomator
    #veracrypt

)

echo "### Installing cask apps ..."
brew cask install ${CASKS[@]}

echo "### Installing fonts ..."
brew tap caskroom/fonts
FONTS=(
    font-fira-code
    font-menlo-for-powerline
)
brew cask install ${FONTS[@]}

#echo "### Installing global npm packages ..."
#yarn global add serve npm-check-updates


#echo "### Configuring ZSH and Hyper ..."
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

##  Install spaceship theme
git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"

## Install one-dark theme
#hyper i hyper-one-dark


#echo "### Creating SSH key ..."
#ssh-keygen -t rsa -C "hky@" -b 4096 -f ~/.ssh/id_rsa -q -N ""


echo "### Configuring git ..."
git config --global user.name "hky"
git config --global user.email "hky@"


#echo "### Configuring OSX ..."

## Require password as soon as screensaver or sleep mode starts
#defaults write com.apple.screensaver askForPassword -int 1
#defaults write com.apple.screensaver askForPasswordDelay -int 0

## FIXME: Don't work right now. Top right screen corner → Start screen saver"
#defaults write com.apple.dock wvous-bl-corner -int 5
#defaults write com.apple.dock wvous-bl-modifier -int 0

# Show filename extensions by default
#defaults write NSGlobalDomain AppleShowAllExtensions -bool true

## Enable tap-to-click
#defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
#defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
#defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

echo "### Bootstrap completed. Happy Hacking!"

#echo "Set shell: '/bin/zsh' in your .hyper.js"
#echo "Set ZSH_THEME=\"spaceship\" in your .zshrc."
