"plug install
"curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

"call plug#begin()
"Plug 'andymass/vim-matchup'
"Plug 'tpope/vim-sensible'
"Plug 'junegunn/vim-easy-align'
"Plug 'itchyny/lightline.vim'
""Plug 'bling/vim-airline'

"Plug 'junegunn/vim-easy-align'

"Plug 'preservim/nerdtree'
"Plug 'preservim/nerdcommenter'

"Plug 'tpope/vim-surround'

"call plug#end()
"------

set nocompatible
"set background=dark
set backspace=2         " backspace in insert mode works like normal editor
"set backspace=eol,start,indent
set autoindent          " auto indenting
set number              " line numbers
set nobackup            " get rid of anoying ~file
set cursorline          " show location of cursor using a horizontal line.

set tabstop=2
set shiftwidth=2
set expandtab
set smarttab
set showmatch

syntax on           " syntax highlighting
set laststatus=2    " Always show status line
set ttyfast         " Optimize for fast terminal connections

"set listchars=tab:>-
"set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
"set list

"inoremap jj <ESC>

set t_Co=256
colorscheme torte

" Strip trailing whitespace (,ss)
function! StripWhitespace()
        let save_cursor = getpos(".")
        let old_query = getreg('/')
        :%s/\s\+$//e
        call setpos('.', save_cursor)
        call setreg('/', old_query)
endfunction
noremap <leader>ss :call StripWhitespace()<CR>

nnoremap <F1> :set number! number?<cr>
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>